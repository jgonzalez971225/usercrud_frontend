import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import AuthLogin from '@/pages/login';

import UserIndex from "@/pages/users";
import UserCreate from "@/components/users/create";
import UserEdit from "@/components/users/edit";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: Home,
    meta:{
      requiredAuth: true
    }
  },
  {
    path: '/login',
    name: "Login",
    component:AuthLogin
  },
  {
    path: '/users',
    name: "users-index",
    component:UserIndex
  },
  {
    path: '/users/create',
    name: "users-create",
    component:UserCreate
  },
  {
    path: '/users/:id/edit',
    name: "users-edit",
    component:UserEdit
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
