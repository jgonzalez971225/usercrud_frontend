import ApiConnection from './api';

export default {
    index() {
        return ApiConnection().get('v1/users');
    },
    show(id) {
        return ApiConnection().get('v1/users/' + id);
    },
    store(data) {
        return ApiConnection().post('v1/users', data);
    },
    update(id,data) {
        return ApiConnection().put('v1/users/'+id, data);
    },
    delete(id) {
        return ApiConnection().delete('v1/users/' + id);
    }
}