import ApiConnection from './api';

export default {
    index() {
        return ApiConnection().get('v1/roles');
    },
}