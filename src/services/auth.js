import ApiConnection from './api';

export default {

    auth(credentials) {
        return new Promise((resolve, reject) => {
            ApiConnection().post('v1/auth', credentials)
                .then((response) => {
                    const token = response.data.access_token;
                    localStorage.setItem('access_token', token);
                    resolve(token);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    profile() {
        return new Promise((resolve, reject) => {
            ApiConnection().get('/v1/user-profile').then((response) => {
                resolve(response);
            }).catch(error => {
                reject(error);
            });
        });
    },

    logout() {
        return new Promise((resolve, reject) => {
            ApiConnection().post('/api/v1/auth/logout')
                .then(response => {
                    localStorage.removeItem('access_token')
                    resolve(response)
                })
                .catch(error => {
                    localStorage.removeItem('access_token')
                    reject(error)
                });
        })
    }

}