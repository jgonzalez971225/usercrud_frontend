export default() => {

    let token = localStorage.getItem('access_token');

    let params = {
        'baseURL':"http://127.0.0.1:8000/api/",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization':'Bearer '+token
        }
    };

    return axios.create(params);

}