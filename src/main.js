require('./bootstrap');
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

//TODO:MOVE TO UNIQUE FILE
router.beforeEach(async (to, from, next) => {
    if (store.getters.isAuth)
        await store.dispatch('freshProfile');

    if (to.matched.some(record => record.meta.requiredAdmin)) {
        if (store.getters.isAuth && store.getters.isUserAdmin) {
            next();
        }else{
            next({name:'404'});
        }
    }

    next();
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
